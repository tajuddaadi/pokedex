import React, { useState } from "react";
import { useQuery } from "@apollo/client";
import { GET_POKEMONS } from "./queries/get-pokemons";
import { PokemonData, PokemonVars } from "./types/pokemon";
import PokemonCard from "./components/PokemonCard";

function App() {
  const [filter, setFilter] = useState("");
  const { loading, error, data } = useQuery<PokemonData, PokemonVars>(
    GET_POKEMONS,
    {
      variables: {
        type: filter,
      },
    }
  );

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const handleFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFilter(event.target.value);
  };

  return (
    <div className="App">
      <h1>Pokémon Browser</h1>
      <div>
        <label>Filter by type:</label>
        <input type="text" value={filter} onChange={handleFilterChange} />
      </div>
      <div className="card-container">
        {data?.pokemons.map((pokemon) => (
          <PokemonCard key={pokemon.id} pokemon={pokemon} />
        ))}
      </div>
    </div>
  );
}

export default App;