import React from "react";
import { Pokemon } from "../types/pokemon";

type Props = {
  pokemon: Pokemon;
};

function PokemonCard({ pokemon }: Props) {
  return (
    <div className="card">
      <div className="card-image-container">
        <img src={pokemon.image} alt={pokemon.name} className="card-image" />
      </div>
      <div className="card-content">
        <h2 className="card-title">{pokemon.name}</h2>
        <div className="card-types">
          {pokemon.types.map((type) => (
            <span key={type} className={`card-type ${type.toLowerCase()}`}>
              {type}
            </span>
          ))}
        </div>
      </div>
    </div>
  );
}

export default PokemonCard;