import { gql } from "@apollo/client";

export const GET_POKEMONS = gql`
  query GetPokemons($type: String!) {
    pokemons(type: $type) {
      id
      name
      image
      types
    }
  }
`;