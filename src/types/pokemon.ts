export type Pokemon = {
    id: string;
    name: string;
    image: string;
    types: string[];
  };
  
  export type PokemonData = {
    pokemons: Pokemon[];
  };
  
  export type PokemonVars = {
    type: string;
  };